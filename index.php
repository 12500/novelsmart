<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <title>NOVELSMART</title>
  <meta property="og:site_name" content="Gogoro" />
  <meta property="og:type" content='website' />
  <meta name="description" content="Our society is at a tipping point. We need bold solutions and a mindset for change for the next generation. It’s time to rethink how the growing cities of tomorrow distribute, manage and experience energy.">
  <meta property="og:type" content="website">
  <meta property="og:title" content="Gogoro Global">
  <meta property="og:description" content="Our society is at a tipping point. We need bold solutions and a mindset for change for the next generation. It’s time to rethink how the growing cities of tomorrow distribute, manage and experience energy.">
  <meta property="og:url" content="/">
  <meta property="og:image" content="uploads/pages/2-og-image-71f725242631ab1ba319e758887d64a33fbec42ce903.png?1540538704">
  <!-- <script src="js/ggr-dls.min.js"></script> -->
  <script>process = { env: { locale: 'en', MY_GOGORO_DOMAIN: 'my.gogoro.com' }, browser: true }</script>
  <!-- <script defer src="{% static 'main/js/ggr-components.min.js' %}"></script>
 -->
    <?php include 'header.php' ?>
</head>

<body class="landing-page no-padding-top">

 <!-- <ggr-nav :scrolling-opacity="true" theme="light" :url-prefix="false"></ggr-nav> -->

<?php include 'navbar.php' ?>
<div class="section-hero">
  <div class="hero-container">
    <div class="hero-list">
        <div class="hero hero-viva" data-theme="light">
           <figure class="hero-background"></figure>
          <div class="viva-content-layer">
            <div class="viva-content">
                <h1 class="viva-banner-title" style=" text-shadow: 0px 2px 2px rgba(0, 0, 0, 0.4);"><b>PURE ELECTRIC MOTION</b></h1>
                <p class="viva-banner-subtitle">Introducing <b>Premium</b> <span="slogan-zh">Electric Bikes</span></p>
                <div class="viva-banner-actions">
                    <a class="viva-action-btn" href="#frm">Request Test Drive</a>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>

</div>

<section class="landing-page-section section-campaigns">
  <ul class="campaign-list campaign-list--2">
      <li class="campaign-item" >
        <a class="campaign
        campaign--light
        campaign--top" href="#" target="_self">
          <div class="campaign-copy-wrapper">
            <div class="campaign-copy">
              <h2 class="campaign-title"><strong>No Maintenance. No Problem</strong></h2>
              <div class="campaign-subtitle"></div>
            </div>
          </div>
          <span class="campaign-bg1"></span>
        </a>
      </li>
      <li class="campaign-item" >
        <a class="campaign
        campaign--dark
        campaign--top" href="#" target="_self">
          <div class="campaign-copy-wrapper">
            <div class="campaign-copy">
              <h2 class="campaign-title" style="color:white;"><strong>Built with style, equiped to perform!</strong></h2>
              <div class="campaign-subtitle" style="color:white;"></div>
            </div>
          </div>
          <span class="campaign-bg"><img width="625px" height="auto" src="img/bike.jpg"></span>
        </a>
      </li>
  </ul>
</section>

<section class="landing-page-section section-smart-power">
  <div class="container text-center text-lg-left">
    <div class="row d-lg-flex">
      <div class="col-lg-5 col-lg-offset-1">
        <div class="row">
          <div class="col-lg-12 col-lg-offset-0 col-md-8 col-md-offset-2">
            <div class="smart-power-copy">
              <h2 class="smart-power-title">Safe, Smart, Affordable</h2>
              <p class="smart-power-intro">Built with the latest anti-theft tracking system to protect, compliment and power your ride <span data-dynamic-var="stationsCount"></span> electric POWER</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-lg-offset-0 col-md-8 col-md-offset-2">
            <div class="smart-power-badge">
              <div class="smart-power-badge__value">
                <!-- <span data-dynamic-var="stationsCount"></span> -->
              </div>
          </div>
          </div>
        </div>
        <div class="smart-power-actions">
          <a href="#frm" class="smart-power-link smart-power-primary-link">
            <span class="smart-power-link-text">I am Interested</span>
          </a>
        </div>
      </div>
      <div style="" class="col-lg-5 col-lg-offset-0 col-md-8 col-md-offset-2">
        <div class="row">
          <div class="col-lg-12 col-lg-offset-0 col-md-8 col-md-offset-2">
            <div class="smart-power-badge">
              <div class="smart-power-badge__caption">
                <h2 class="smart-power-title">Calculate your daily cost to and from work</h2>
              </div>
            </div>
        </div>
        </div>


          <div  class="col-md-12 float-center wow animated fadeInRight" data-wow-delay=".2s">
            <div class="row">
                <div class="col-lg-12 col-lg-offset-0 col-md-8 col-md-offset-2">
                  <h2><strong class="smart-power-intro">Enter your distance from home to work (km)</strong></h2>
                  <div class="impact-desc form-group label-floating">
                    <input onkeypress="return isNumberKey(event)" class="numbersonly" style="font-size:26px;text-align: center;border:4px solid #eaf6ea" id="distance" oninput="getPrice(this.value)" onchange="getPrice(this.value)" type="text" placeholder ="Distance (km)">
                  </div>
                  <span><h1><strong style="color:green;" class="smart-power-intro">Your daily travel expense will be:</strong></h1><br><h3 class="smart-power-intro" style="font-size:20px;" id="demo"><strong></strong></h3></span>
                </div>
            </div>
          </div>



      </div>
      </div>
    </div>

  </div>
</section>

<section class="landing-page-section section-media">
  <div class="media-container-wrapper">
    <div class="media-container blog-posts-container">
      <div class="media-section-name">NOVEL X1 Specs</div>
      <ul class="blog-post-list">
        <li class="social-post-item">
  <a class="social-post social-post--light" target="_blank" href="#">
    <div class="social-post-copy-wrapper">
      <div class="social-post-copy">
        <h3 class="social-post-title media-item-title">
          New standard of reliability
        </h3>
        <div class="social-post-subtitle"></div>
      </div>
    </div>
    <span class="social-post-bg"><img src="img/bike.jpg"></span>
  </a>
</li>

      </ul>
    </div>
    <div class="media-container social-media-container">
      <div class="media-section-name">
        <div class="social-post-icon">
          <!-- <i class="fa fa-instagram"></i> -->
        </div>
      </div>
      <ul class="social-post-list">
        <li class="social-post-item">
  <a class="social-post social-post--" target="_blank" href="#">
    <div class="social-post-copy-wrapper">
      <div class="social-post-copy">
        <h3 class="social-post-title media-item-title">
          Ride through Kampala
        </h3>
        <div class="social-post-subtitle">Stories from our community</div>
      </div>
    </div>
    <span class="social-post-bg"><img src="img/social-post-2.jpg"></span>
  </a>
</li>

      </ul>
    </div>
  </div>
</section>

  <section class="section-quotes landing-page-section">
  <div class="quote-text-list">
    <a target="_blank" class="quote-link" href="#">
      <blockquote>
        <q class="quote-text">You have to see it to really appreciate how cool it is,</q>
        <footer>
          <cite class="quote-cite">Al Gore, Wired, December 2017</cite>
        </footer>
      </blockquote>
    </a>
    <a target="_blank" class="quote-link" href="#">
      <blockquote>
        <q class="quote-text">The 9 Coolest Gadgets from CES 2016</q>
        <footer>
          <cite class="quote-cite">Time</cite>
        </footer>
      </blockquote>
    </a>
    <a target="_blank" class="quote-link" href="#">
      <blockquote>
        <q class="quote-text">2018 Asian Company of the Year</q>
        <footer>
          <cite class="quote-cite">Cleantech 100</cite>
        </footer>
      </blockquote>
    </a>
    <a target="_blank" class="quote-link" href="#">
      <blockquote>
        <q class="quote-text">2015 CES Best In Show Award</q>
        <footer>
          <cite class="quote-cite">The Verge</cite>
        </footer>
      </blockquote>
    </a>
    <a target="_blank" class="quote-link" href="#">
      <blockquote>
        <q class="quote-text">2017 Nikkei Asian Review Award for Excellence</q>
        <footer>
          <cite class="quote-cite">Nikkei Asian Review</cite>
        </footer>
      </blockquote>
    </a>
    <a target="_blank" class="quote-link" href="#">
      <blockquote>
        <q class="quote-text">Selected for the 2016 Sustainia 1000</q>
        <footer>
          <cite class="quote-cite">Sustainia</cite>
        </footer>
      </blockquote>
    </a>
  </div>
  <div class="quote-logo-list">
    <div class="quote-logo-item">
      <div class="quote-logo quote-logo--wired"></div>
    </div>
    <div class="quote-logo-item">
      <div class="quote-logo quote-logo--time"></div>
    </div>
    <div class="quote-logo-item">
      <div class="quote-logo quote-logo--cleantech"></div>
    </div>
    <div class="quote-logo-item">
      <div class="quote-logo quote-logo--verge"></div>
    </div>
    <div class="quote-logo-item">
      <div class="quote-logo quote-logo--nikkei"></div>
    </div>
    <div class="quote-logo-item">
      <div class="quote-logo quote-logo--sustania"></div>
    </div>
  </div>
</section>


<section class="landing-page-section section-impact">

  <div class="container">
    <div class="impact-headline">Our community’s impact</div>
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="impact-item-list">
          <div class="impact-item active">
            <div class="impact-animation impact-animation--co2">
              <div class="impact-co2-cloud"></div>
              <div class="impact-co2-arrow"></div>
              <div class="impact-co2-tree"></div>
            </div>
            <div class="impact-badge">
              <span class="impact-value">
                <span class="impact-value" data-dynamic-var="CO2">&nbsp;</span>
                <span class="impact-unit">KG</span>
              </span>
              <div class="impact-caption">CO2 saved</div>
              <div class="impact-desc">As many as the amount of CO2 that <span data-dynamic-var="CO2" data-dynamic-eval="Math.floor($/10)"></span> trees consume every year.</div>
            </div>
          </div>

          <div class="impact-item">
            <div class="impact-animation impact-animation--batteries">
              <div class="impact-battery-charger">
                <div class="impact-battery-seat impact-battery-seat--left">
                  <div class="impact-battery">
                    <div class="impact-battery-energy"></div>
                  </div>
                </div>
                <div class="impact-battery-seat impact-battery-seat--right">
                  <div class="impact-battery">
                    <div class="impact-battery-energy"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="impact-badge">
                <span class="impact-value">
                  <span class="impact-value" data-dynamic-var="Swap">&nbsp;</span>
                  <span class="impact-unit"></span>
                </span>
              <div class="impact-caption">Batteries swapped</div>
              <div class="impact-desc">There are more than <span data-dynamic-var="SwapAvg30Days" data-dynamic-eval="Math.floor($)"></span> batteries swapped per day in the last 30 days.</div>
            </div>
          </div>

          <div class="impact-item">
            <div class="impact-animation impact-animation--earth">
              <div class="impact-earth"><div class="impact-earth-track"></div></div>
            </div>
            <div class="impact-badge">
                <span class="impact-value">
                  <span class="impact-value" data-dynamic-var="Mileage">&nbsp;</span>
                  <span class="impact-unit">KM</span>
                </span>
              <div class="impact-caption">Total distance covered</div>
              <div class="impact-desc">As long as the distance traveling around the Earth via equator <span data-dynamic-var="Mileage" data-dynamic-eval="Math.floor($/40000)"></span> times.</div>
            </div>
          </div>
        </div>
        <div class="impact-dots">
          <div class="impact-dot active"></div>
          <div class="impact-dot"></div>
          <div class="impact-dot"></div>
        </div>
      </div>
    </div>
  </div>


</section>

<section id="frm" class="Material-contact-section section-padding ">
  <div class="container" id="contactsection">
      <div class="row">
          <!-- Section Titile -->
          <div class="col-md-12 wow animated fadeInLeft" data-wow-delay=".2s">
              <h1 class="impact-headline">Love to Hear From You</h1>
          </div>
      </div>
      <div class="row">
          <!-- Section Titile -->
          <div class="col-md-6 mt-3 contact-widget-section2 wow animated fadeInLeft" data-wow-delay=".2s">
            <p class="impact-desc">Going electric is bringing travel in and around Kampala back in your power. Mixing cool, efficient and cost effective in one melanged phrase</p>

            <div class="find-widget impact-desc">
            <a href="#">NovelSmart Electric Motors</a>
            </div>
            <div class="find-widget impact-desc">
             <a href="#">Conrad House, Jinja road</a>
            </div>
            <div class="find-widget impact-desc">
             <a href="#">+256782081219</a>
            </div>

            <!-- <div class="find-widget impact-desc">
              Website:  <a href="#">www.uny.ro</a>
            </div> -->
            <div class="find-widget impact-desc">
               Open: <a href="#">Mon to Sat: 09:30 AM - 6:00 PM</a>
            </div>
          </div>
          <!-- contact form -->
          <div class="col-md-6 wow animated fadeInRight" data-wow-delay=".2s">
              <form class="shake" role="form" method="post" id="contactForm" name="contact-form" data-toggle="validator">
                  <!-- Name -->
                  <div class="impact-desc form-group label-floating">
                    <label class="impact-desc control-label" for="name">Name</label>
                    <input class="form-control" id="name" type="text" name="name" required data-error="Please enter your name">
                    <div class="help-block with-errors"></div>
                  </div>
                  <!-- email -->
                  <div class="impact-desc form-group label-floating">
                    <label class="control-label" for="email">Email</label>
                    <input class="form-control" id="email" type="email" name="email" required data-error="Please enter your Email">
                    <div class="help-block with-errors"></div>
                  </div>
                  <!-- Subject -->
                  <div class="form-group label-floating">
                    <label class="control-label">Subject</label>
                    <input class="form-control" id="msg_subject" type="text" name="subject" required data-error="Please enter your message subject">
                    <div class="help-block with-errors"></div>
                  </div>
                  <!-- Message -->
                  <div class="form-group label-floating">
                      <label for="message" class="control-label">Message</label>
                      <textarea class="form-control" rows="3" id="message" name="message" required data-error="Write your message"></textarea>
                      <div class="help-block with-errors"></div>
                  </div>
                  <!-- Form Submit -->
                  <!-- <div class="form-submit mt-5">
                      <button class="btn btn-common" type="submit" id="form-submit"><i class="material-icons mdi mdi-message-outline"></i> Send Message</button>
                      <div id="msgSubmit" class="h3 text-center hidden"></div>
                      <div class="clearfix"></div>
                  </div> -->
                  <div class="smart-power-actions">
                    <a href="#" class="smart-power-link smart-power-primary-link">
                      <span class="smart-power-link-text">Send Message</span>
                    </a>
                  </div>
              </form>
          </div>
      </div>
  </div>
</section>

<script src="js/TweenMax.min.js"></script>

  <!-- <ggr-directory :url-prefix="false"></ggr-directory>

  <ggr-footer :url-prefix="false"></ggr-footer>

  <ggr-locale-switcher></ggr-locale-switcher>

    <ggr-gdpr strategy="always"></ggr-gdpr> -->

  <script src="js/extras.js"></script>
  <script src="js/main.js"></script>
  <script src="js/style.js"></script>
  <script src="js/main2.js"></script>
</body>
</html>
