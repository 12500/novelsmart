<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <title>NOVELSMART - BIKES</title>
  <meta property="og:site_name" content="Gogoro" />
  <meta property="og:type" content='website' />
  <meta name="description" content="The New Gogoro S2 ABS equipped with BOSCH ABS Brake System. Deliver upgraded performance for you to redefine the riding experience!">
  <meta property="og:type" content="website">
  <meta property="og:title" content="Gogoro S2 ABS - UNDEFINED">
  <meta property="og:description" content="The New Gogoro S2 ABS equipped with BOSCH ABS Brake System. Deliver upgraded performance for you to redefine the riding experience!">
  <meta property="og:image" content="img/bj/og-image.png">
  <script src="js/ggr-dls.min1.js"></script>
  <script>process = { env: { locale: 'en', MY_GOGORO_DOMAIN: 'my.gogoro.com' }, browser: true }</script>
  <script defer src="js/ggr-components1.min.js"></script>
  <link rel="stylesheet" media="screen" href="css/main1.css"/>
</head>
<body>
  <ggr-nav :scrolling-opacity="true" theme="dark" :url-prefix="false"></ggr-nav>
<main class="section-list" class="container-fluid">
  <section class="section section-hero">
  <div class="hero-section-container">
    <div class="kv-bjl">
      <div class="kv-bg-canvas"></div>
      <div class="kv-main-canvas"></div>
    </div>
    <div class="hero-copy">
      <div class="container">
        <div class="hero-tag">#Undefined</div>
        <h1 class="hero-title">NovelSmart <span class="hero-title__series">S2 ABS</span></h1>
        <div class="hero-subtitle"></div>
        <div class="hero-action">
          <a class="btn-bj hero-action-btn" href="https://www.gogoro.com/press/updates/">
            <span>Interested</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

  <section class="section section-video section-video--first progressive-image">
    <video class="video-full d-none d-sm-block" src="http://cdn.gogoro.com/resources/images/s-performance/s2/superfast_desktop.mp4" loop autoplay playsinline muted></video>
    <video class="video-full d-sm-none" src="http://cdn.gogoro.com/resources/images/s-performance/s2/mobile/superfast_mobile.mp4" loop autoplay playsinline muted></video>

</section>

  <section class="section section-performance progressive-image">
  <div class="subsection subsection-fast">
    <canvas class="performance-fast-canvas"></canvas>

    <div class="container">
      <div class="row justify-content-center text-center">
        <div class="col-xl-6 col-lg-8 col-md-10">
          <div class="dash-line">
    <div class="dash-line-top"></div>
    <div class="dash-line-bottom"></div>
  </div>
    <h2 class="section-title section-title--animation">Like a bullet.</h2>
    <p class="section-description">There’s fast, then there’s electric fast. A 125cc class electric two wheels with superior performance. You’ve never ridden anything like this.</p>
        </div>
      </div>
    </div>

    <div class="key-number-wrapper">
      <div class="key-number-list">
        <div class="key-number__bg">
          <div class="key-number-sizer"></div>
        </div>
        <div class="key-number-item">
          <div class="key-number-sizer">
            <div class="key-number d-flex flex-column justify-content-between">
              <div class="d-flex align-items-end">
                <div class="key-number__value">3.8</div>
                <div class="key-number__unit">SEC</div>
              </div>
              <div class="key-number__content">0-50 KM/H<br>ACCELERATION</div>
            </div>
          </div>
        </div>
        <div class="key-number-item">
          <div class="key-number-sizer">
            <div class="key-number d-flex flex-column justify-content-between">
              <div class="d-flex align-items-end">
                <div class="key-number__value">7.6</div>
                <div class="key-number__unit">KW at<br>3,000 RPM</div>
              </div>
              <div class="key-number__content">ENHANCED<br>MOTOR OUTPUT</div>
            </div>
          </div>
        </div>
        <div class="key-number-item">
          <div class="key-number-sizer">
            <div class="key-number d-flex flex-column justify-content-between">
              <div class="d-flex align-items-end">
                <div class="key-number__value">213</div>
                <div class="key-number__unit">NM at<br>the wheel</div>
              </div>
              <div class="key-number__content">Extreme<br>Torque</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="subsection subsection-s">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-xl-5 col-lg-5 col-md-4 s-logo-column">
          <figure class="s-logo"></figure>
        </div>

        <div class="col-xl-5 offset-xl-1 col-lg-6 offset-lg-1 col-md-7 offset-md-1 s-content-column">
          <div class="s-content">
            <h2 class="section-title section-title--animation">The mark that redefined electric</h2>
            <p class="section-description">Enhanced in speed, performance, and riding mechanics, Gogoro S Performance vehicles are designed to giving you an electrifying trip everytime you ride. Riders be warned, the thrill is seriously addictive.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="subsection subsection-chart">
    <div class="container">
      <div class="row align-items-center flex-column-reverse flex-lg-row">
        <div class="col-xl-4 offset-xl-1 col-lg-5 chart-content-column">
          <h3 class="section-headline">Exhilarating</h3>
          <p class="section-description">Be the first off the line by unleash all the torque in an instant, this is a white knuckle experience only electric can provide.</p>
        </div>
        <div class="offset-xl-1 col-xl-6 col-lg-7 chart-graphic-column">
          <div class="s-chart">
            <div class="s-chart__coord">
              <div class="s-chart__data-list">
                <div class="s-chart__s2">
                  <div class="s-chart__name">Novel X1</div>
                  <div class="s-chart__bar"></div>
                  <div class="s-chart__result"><span class="s-chart__result-value">3.8</span> <span class="s-chart__result--unit">sec</span></div>
                </div>
                <div class="s-chart__125">
                  <div class="s-chart__name">125CC</div>
                  <div class="s-chart__bar"></div>
                  <div class="s-chart__result"><span class="s-chart__result-value">4.7</span> <span class="s-chart__result--unit">sec</span></div>
                </div>
              </div>
              <div class="s-chart__axis-list">
                <div class="s-chart__axis">
                  <span class="s-chart__axis-text">0 <span class="s-chart__axis-text-unit">KMH</span></span>
                </div>
                <div class="s-chart__axis">
                  <span class="s-chart__axis-text">10 <span class="s-chart__axis-text-unit">KMH</span></span>
                </div>
                <div class="s-chart__axis">
                  <span class="s-chart__axis-text">20 <span class="s-chart__axis-text-unit">KMH</span></span>
                </div>
                <div class="s-chart__axis">
                  <span class="s-chart__axis-text">30 <span class="s-chart__axis-text-unit">KMH</span></span>
                </div>
                <div class="s-chart__axis">
                  <span class="s-chart__axis-text">40 <span class="s-chart__axis-text-unit">KMH</span></span>
                </div>
                <div class="s-chart__axis">
                  <span class="s-chart__axis-text">50 <span class="s-chart__axis-text-unit">KMH</span></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

  <section class="section section-safety progressive-image">
  <div class="subsection subsection-safety">
    <figure class="dash-bg dash-bg--right"></figure>
    <div class="container h-100">
      <div class="row justify-content-center align-items-center h-100">
        <div class="col-xl-6 col-lg-8 col-md-10 text-center">
          <div class="safety-header">
            <div class="dash-line">
  <div class="dash-line-top"></div>
  <div class="dash-line-bottom"></div>
</div>

            <h2 class="section-title section-title--animation">Ride safer, play harder.</h2>
            <p class="section-description">Equipped with the Bosch 10 anti-lock brake system<sup>*1</sup> and exclusive MAXXIS ABS Tires<sup>*1</sup>, keep the ride smooth no matter how hard you want to play.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="subsection subsection-safety-feature">
    <figure class="dash-bg dash-bg--left"></figure>
    <div class="feature-card">
      <div class="container">
        <div class="row">
          <div class="col-xl-8 col-md-10">
            <figure class="feature-image  feature-image--abs"></figure>
            <div class="feature-content">
              <h3 class="section-headline">BOSCH ABS brake system<sup>*1</sup></h3>
              <p class="section-description">Also found in heavy duty motorcycles, the Bosch 10 ABS system monitors the front and rear wheel at a frequency of 200 times per second to prevent an imminent wheel lock. Weighs only 580-gram, this device balances performance and safety in all riding situations.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="feature-card">
      <div class="container">
        <div class="row">
          <div class="col-xl-8 offset-xl-5 col-md-10 offset-md-2">
            <figure class="feature-image feature-image--tire"></figure>
            <div class="feature-content">
              <h3 class="section-headline">MAXXIS ABS Tires<sup>*1</sup></h3>
              <p class="section-description">Maxxis M6237 Street performance tires provide enhanced control on slippery road surfaces, so you can go faster, further and safer.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="feature-card">
      <div class="container">
        <div class="row">
          <div class="col-xl-8 col-md-10">
            <figure class="feature-image  feature-image--suspension"></figure>
            <div class="feature-content">
              <h3 class="section-headline">Adjustable suspensions</h3>
              <p class="section-description">A pair of thicker, more robust front suspensions, coupled with custom dampening controls, provide the comfort in speed so you can to take on anything.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

  <section class="section section-break progressive-image">
  <div class="break-layer">

    <div class="image-break-scooter-placeholder"></div>

    <div class="break-bg-sticky-container">
      <div class="break-bg"></div>
    </div>

    <div class="break-scooter-sticky">
      <figure class="image-break-scooter">
        <div class="break-top-text"></div>
      </figure>
    </div>

    <div class="break-text-group">
      <div class="break-text-container">
        <figure class="image-break-text image-break-text--1"></figure>
      </div>
      <div class="break-text-container">
        <figure class="image-break-text image-break-text--3"></figure>
      </div>
      <div class="break-text-container">
        <figure class="image-break-text image-break-text--2"></figure>
      </div>
    </div>

    <div class="break-content d-none d-md-block">
      <div class="container">
        <div class="break-content-sizer">
          <h3 class="section-headline">Breakthrough the boundary of color- Spectrum Indigo<sup>*1</sup></h3>
          <p class="section-description">A simple but delicate look with multi-layered pearlescent finish. The colors change depending on the light source, viewing angle and its surroundings. The color-shifting purple, magenta and red further complements the streamlined exterior. Challenge your imagination. It’s understatement redefined with intricacies that will blow your mind.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="break-content break-content--outside d-md-none">
    <div class="container">
      <div class="break-content-sizer">
        <h3 class="section-title">Breakthrough the boundary of color- Spectrum Indigo<sup>*1</sup></h3>
        <p class="section-description">A simple but delicate look with multi-layered pearlescent finish. The colors change depending on the light source, viewing angle and its surroundings. The color-shifting purple, magenta and red further complements the streamlined exterior. Challenge your imagination. It’s understatement redefined with intricacies that will blow your mind.</p>
      </div>
    </div>
  </div>
</section>

  <section class="section section-video section-video--second progressive-image">
    <video class="video-full d-none d-sm-block" src="http://cdn.gogoro.com/resources/images/s-performance/s2/bj/beauty_video_BJL.mp4" loop autoplay playsinline muted></video>
    <video class="video-full d-sm-none" src="http://cdn.gogoro.com/resources/images/s-performance/s2/bj/mobile_beauty_video_BJL.mp4" loop autoplay playsinline muted></video>

</section>

  <section class="section section-part progressive-image">
  <div class="subsection subsection-part">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-6 col-lg-8 col-md-10 text-center">
          <div class="dash-line">
  <div class="dash-line-top"></div>
  <div class="dash-line-bottom"></div>
</div>

          <h2 class="section-title section-title--animation">Raw beauty.</h2>
          <p class="section-description">Eyeball grabbing even in the darkest night, the Gogoro S2 sports a series of exterior upgrades that’s visually striking enough to stun.</p>
        </div>
      </div>
    </div>
    <figure class="image-part image-part--front">
      <div class="part-desc-origin">
        <div class="part-desc part-desc--shield">RACER STYLE<br>WIND SHIELD</div>
        <div class="part-desc part-desc--handlebar">Naked<br>Handlebar</div>
      </div>
    </figure>
    <div class="container part-grid-container">
      <div class="row no-gutters">
        <div class="col-sm-6">
          <figure class="image-part image-part--seat">
            <div class="part-desc-origin"><div class="part-desc part-desc--seat">S Performance<br>Friction<br>Enhanced Seat</div></div>
          </figure>
        </div>
        <div class="col-sm-6">
          <figure class="image-part image-part--badge">
            <div class="part-desc-origin"><div class="part-desc part-desc--badge">S Performance<br>Seat Badge</div></div>
          </figure>
        </div>
        <div class="col-sm-6">
          <figure class="image-part image-part--grip">
            <div class="part-desc-origin"><div class="part-desc part-desc--grip">Streamlined<br>Tail Grip</div></div>
          </figure>
        </div>
        <div class="col-sm-6">
          <figure class="image-part image-part--box">
            <div class="part-desc-origin"><div class="part-desc part-desc--box"><div><div>Front Storage</div><div>Compartment<sup>*2</sup></div></div></div></div>
          </figure>
        </div>
      </div>
    </div>
  </div>
  <div class="subsection subsection-shadow">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-6 col-lg-8 col-md-10 text-center">
          <h2 class="section-title section-title--animation">A legacy that begins in <br class="d-none d-md-block">the shadow.</h2>
          <p class="section-description">Inspired by the coolness of shadow and graphite’s muted texture, Gogoro S2 comes in black and black only for the most obvious reason. Enhance your cool look no matter which path you choose to take on.</p>
        </div>
      </div>
    </div>
  </div>
</section>

  <section class="section section-station progressive-image">
  <div class="station-content">
    <div class="container">
      <div class="row justify-content-between align-items-center">
        <div class="col-xl-7 col-md-8">
          <h2 class="section-title">Quick, Swap, Go!</h2>
          <p class="section-description"><span data-dynamic-var="stationsCount"></span>+ GoStation® at your service. Simply swap to a pair of charged batteries in under 6 seconds.</p>
        </div>
        <div class="col-md-auto">
          <a class="station-btn" href="https://www.gogoro.com/gogoro-network/" target="_blank">
            <span>FIND US</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

  <section class="section section-gallery">
  <div class="easter-egg-layer">

  </div>
  <button class="gallery-btn gallery-btn--left"></button>
  <button class="gallery-btn gallery-btn--right"></button>
  <canvas class="gallery-canvas">
  </canvas>
</section>

  <section class="section-build progressive-image">
  <div class="section-content">
    <input type="radio" name="model" class="d-none model-toggle" id="model-sbs" checked>
    <input type="radio" name="model" class="d-none model-toggle" id="model-abs">

    <div class="models">
        <div class="model">

          <div class="model-images">
            <figure class="image-model image-model--sbs">
            </figure>
          </div>

          <div class="model-color-badge model-color-badge--carbon">Graphite Grey</div>

          <div class="model-product-name-wrapper">
            <label for="model-sbs" class="model-product-name">
              Gogoro <span class="model-name">S2</span>
            </label>
          </div>

          <div class="model-body">
            <div class="model-price cta-price">

            </div>

            <ul class="model-features">
                <li>7.6 kW @ 3,000 rpm</li>
                <li>SBS (Synchronized Brake System)</li>
                <li>Front Maxxis M6237 SPORT tire <br> Rear Maxxis M6237 hybrid dual compound tire<br>(for maximum handling and power efficiency)</li>
                <li>USB charging port in trunk</li>
            </ul>

            <a class="model-cta btn-bj" href="https://www.gogoro.com/press/updates/">Interested</a>

          </div>
        </div>
        <div class="model">

          <div class="model-images">
            <figure class="image-model image-model--abs">
            </figure>
          </div>

          <div class="model-color-badge model-color-badge--indigo">Spectrum Indigo</div>

          <div class="model-product-name-wrapper">
            <label for="model-abs" class="model-product-name">
              Gogoro <span class="model-name">S2 ABS</span>
            </label>
          </div>

          <div class="model-body">
            <div class="model-price cta-price">

            </div>

            <ul class="model-features">
                <li>7.6 kW @ 3,000 rpm</li>
                <li>BOSCH 10 ABS (Anti-lock Brake System)</li>
                <li>Front & Rear MAXXIS M6237 STREET tire (designed for ABS)</li>
                <li>USB charging port on handle bar</li>
            </ul>

            <a class="model-cta btn-bj" href="https://www.gogoro.com/press/updates/">Interested</a>

          </div>
        </div>
    </div>
  </div>

</section>

  <section class="section section-specs progressive-image">
  <div class="container">
    <div class="specs-header">
      <div class="specs-headline">SPEC & FEATURES</div>
      <a class="specs-link" href="https://www.gogoro.com/smartscooter/specs/?models=gogoro-s2-abs,gogoro-s2">Compare S Performance Edition</a>
    </div>
    <input type="checkbox" id="specs-toggle" class="d-none">

    <div class="specs-list">
        <div class="specs-item d-sm-flex " style="order: 0">
          <div class="specs-icon specs-icon--color"></div>
          <div class="specs">
            <div class="specs-name">COLORS</div>
            <div class="specs-content">Spectrum Indigo<sup>*1</sup>  / Graphite Grey<sup>*2</sup></div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 1">
          <div class="specs-icon specs-icon--dimension"></div>
          <div class="specs">
            <div class="specs-name">VISOR</div>
            <div class="specs-content">Racer style smoke visor</div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 2">
          <div class="specs-icon specs-icon--wind"></div>
          <div class="specs">
            <div class="specs-name">DIMENSIONS</div>
            <div class="specs-content">1,890 x 660 x 1,080 mm</div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 3">
          <div class="specs-icon specs-icon--trunk"></div>
          <div class="specs">
            <div class="specs-name">TRUNK SIZE</div>
            <div class="specs-content">25 L (Fits 1 full face or 2 open face helmet)</div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 0">
          <div class="specs-icon specs-icon--KW"></div>
          <div class="specs">
            <div class="specs-name">MAX. POWER OUTPUT</div>
            <div class="specs-content">7.6kW @ 3,000 rpm</div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 1">
          <div class="specs-icon specs-icon--accel"></div>
          <div class="specs">
            <div class="specs-name">MAX. TORGUE (MOTOR / WHEEL)</div>
            <div class="specs-content">26 / 213 Nm @ 0-2,500 rpm</div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 2">
          <div class="specs-icon specs-icon--smart-card"></div>
          <div class="specs">
            <div class="specs-name">LOCK & UNLOCK</div>
            <div class="specs-content">iQ System® Smart Keycard / Smartphone / <br>Apple Watch</div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 3">
          <div class="specs-icon specs-icon--abs"></div>
          <div class="specs">
            <div class="specs-name">BRAKE SYSTEM<sup>*</sup></div>
            <div class="specs-content">ABS (Antilock Brake System)<sup>*1</sup> / <br>SBS (Synchronized Brake System)<sup>*2</sup></div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 0">
          <div class="specs-icon specs-icon--sbs"></div>
          <div class="specs">
            <div class="specs-name">BRAKE CALIPER</div>
            <div class="specs-content">4 Piston radial mounted caliper</div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 1">
          <div class="specs-icon specs-icon--sus"></div>
          <div class="specs">
            <div class="specs-name">SUSPENSIONS</div>
            <div class="specs-content">Adjustable front / rear suspensions</div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 2">
          <div class="specs-icon specs-icon--disk"></div>
          <div class="specs">
            <div class="specs-name">BRAKE ROTOR</div>
            <div class="specs-content">245mm Enlarged front disk</div>
          </div>
        </div>
        <div class="specs-item d-sm-flex " style="order: 3">
          <div class="specs-icon specs-icon--led"></div>
          <div class="specs">
            <div class="specs-name">LED</div>
            <div class="specs-content">LED headlight with halo position light</div>
          </div>
        </div>
      <label class="specs-trigger d-sm-none" for="specs-toggle">KNOW MORE</label>
    </div>
    <div class="specs-disclaimer">
      *1 Only available on Gogoro S2 ABS<br>
*2 Only available on Gogoro S2

    </div>
  </div>
</section>

</main>
<script src="js/libs.js"></script>
<script src="js/hero.min.js"></script>

  <ggr-directory :url-prefix="false"></ggr-directory>

  <ggr-footer :url-prefix="false"></ggr-footer>

  <ggr-locale-switcher></ggr-locale-switcher>

    <ggr-gdpr strategy="always"></ggr-gdpr>

  <script src="js/mainx.js"></script>
    <script src="http://cdn.jsdelivr.net/npm/lottie-web@5.5.7"></script>
  <script src="js/three.min.js"></script>
  <script src="http://cdn.jsdelivr.net/npm/@bincode/infinite-gallery@latest"></script>
  <script src="js/mainy.js"></script>

  <!-- Google Tag Manager -->
<noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-NCBSL8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '../../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NCBSL8');</script>
<!-- End Google Tag Manager -->

</body>

<!-- Mirrored from www.gogoro.com/smartscooter/s-performance/s2/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Dec 2019 09:24:46 GMT -->
</html>
